package ru.t1.chubarov.tm.exception.field;

public final class ExistsEmailException extends AbstractFieldException {

    public ExistsEmailException(String login, String email) {
        super(" Error. Email " + email + "is exists for or login = " + login + ". ");
    }

}
