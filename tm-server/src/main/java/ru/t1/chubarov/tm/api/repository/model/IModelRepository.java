package ru.t1.chubarov.tm.api.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.model.AbstractModel;

@Repository
@Scope("prototype")
public interface IModelRepository<M extends AbstractModel> extends JpaRepository<M,String> {

}
