package ru.t1.chubarov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
public interface ITaskModelRepository extends IModelRepository<Task> {

    void deleteAll();

    void deleteByUserId(@Nullable String userId);

    void deleteById(@Nullable String Id);

    void deleteByUserIdAndId(@Nullable String userId, @Nullable String id);

    long count();

    long countAllByUserId(@Nullable String userId);

    long countAllByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Nullable
    Task findFirstById(@NotNull String id);

    @Nullable
    @Query("SELECT p FROM Task p")
    List<Task> findAll();

    @Nullable
    List<Task> findAllByUserId(@Nullable String userId);

    @Nullable
    List<Task> findAllByUserIdAndId(@Nullable String userId, @Nullable String id);

}
