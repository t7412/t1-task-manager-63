package ru.t1.chubarov.tm.repository;

import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("ONE"));
        add(new Task("TWO"));
    }

    public void create() {
        add(new Task("New task " + System.currentTimeMillis()));
    }

    public void add(Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(Task task) {
        tasks.put(task.getId(), task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(String id) {
        return tasks.get(id);
    }

    public void removeById(String id) {
        tasks.remove(id);
    }

}
