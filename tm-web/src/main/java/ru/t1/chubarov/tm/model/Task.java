package ru.t1.chubarov.tm.model;

import lombok.Getter;
import lombok.Setter;
import ru.t1.chubarov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class Task {

    private String id = UUID.randomUUID().toString();

    private String name = "tsk_"+id.substring(1,4);;

    private String description;

    private Status status = Status.NOT_STARTED;

    private Date dateStart;

    private Date dateFinish = new Date();

    private String projectId;

    public Task(String name) {}

    public Task() {

    }

    public String getId() {
        return id;
    }
}
